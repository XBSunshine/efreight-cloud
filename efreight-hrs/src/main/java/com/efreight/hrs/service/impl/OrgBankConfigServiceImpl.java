package com.efreight.hrs.service.impl;

import com.efreight.hrs.entity.OrgBankConfig;
import com.efreight.hrs.dao.OrgBankConfigMapper;
import com.efreight.hrs.service.OrgBankConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * HRS 签约公司 服务套餐设置 服务实现类
 * </p>
 *
 * @author xiaobo
 * @since 2020-12-29
 */
@Service
public class OrgBankConfigServiceImpl extends ServiceImpl<OrgBankConfigMapper, OrgBankConfig> implements OrgBankConfigService {

}
