package com.efreight.hrs.service;

import com.efreight.hrs.entity.OrgBankConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * HRS 签约公司 服务套餐设置 服务类
 * </p>
 *
 * @author xiaobo
 * @since 2020-12-29
 */
public interface OrgBankConfigService extends IService<OrgBankConfig> {

}
