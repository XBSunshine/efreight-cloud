package com.efreight.hrs.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * HRS 签约公司 服务套餐设置 前端控制器
 * </p>
 *
 * @author xiaobo
 * @since 2020-12-29
 */
@RestController
@RequestMapping("/hrs/org-bank-config")
public class OrgBankConfigController {

}

