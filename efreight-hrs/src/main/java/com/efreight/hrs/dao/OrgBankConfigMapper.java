package com.efreight.hrs.dao;

import com.efreight.hrs.entity.OrgBankConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * HRS 签约公司 服务套餐设置 Mapper 接口
 * </p>
 *
 * @author xiaobo
 * @since 2020-12-29
 */
public interface OrgBankConfigMapper extends BaseMapper<OrgBankConfig> {

}
